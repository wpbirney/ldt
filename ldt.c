#include <stdio.h>
#include <string.h>
#include <time.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

struct response	{
	char buffer[32];
	unsigned int len;
};

struct response create_response()
{
	struct response new_response;
	time_t tp = time(NULL);
	strcpy(&new_response.buffer[0], ctime(&tp));
	new_response.len = strlen(&new_response.buffer[0]);
	return new_response;
}

int main(void)
{
	int r;
	int fd = socket(AF_INET, SOCK_DGRAM, 0);

	struct sockaddr_in hostaddr;
	hostaddr.sin_port = htons(24001);
	hostaddr.sin_family = AF_INET;
	hostaddr.sin_addr.s_addr = INADDR_ANY;

	r = bind(fd, (struct sockaddr*)&hostaddr, sizeof(struct sockaddr_in));
	if(r != 0)
		return -1;

	struct sockaddr recvAddr;
	struct response resp;

	char buffer[256];
	int s;

	while(1)	{
		r = recvfrom(fd, &buffer[0], 256, 0, &recvAddr, &s);
		if(r > 0)	{
			resp = create_response();
			sendto(fd, &resp.buffer[0], resp.len, 0, &recvAddr, s);
		}
	}

	return 0;
}

