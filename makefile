EXECUTABLE=ldt
CC=gcc
CFLAGS=-O3 -s

SOURCES=ldt.c

all:
	$(CC) $(CFLAGS) $(SOURCES) -o $(EXECUTABLE)

clean:
	rm -fv *.o $(EXECUTABLE)
